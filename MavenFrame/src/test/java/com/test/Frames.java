package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;
public class Frames {
	
WebDriver driver;
	
	String ExpectedResult;
	@BeforeClass
	public void Initialize()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/");
		driver.manage().window().maximize();	
	}
	
	@Test(priority=0)
	public void AddTextToFrame() throws InterruptedException
	{
		
		driver.findElement(By.xpath("//a[text()='Frames']")).click();
		

		driver.findElement(By.xpath("//a[text()='iFrame']")).click();
		
		driver.findElement(By.xpath("//span[text()='File']")).click();
		
		
		driver.findElement(By.xpath("//span[text()='New document']")).click();
		driver.switchTo().frame("mce_0_ifr");
		
		
		driver.findElement(By.xpath("//body[@id='tinymce']")).sendKeys("test");
	}

}
