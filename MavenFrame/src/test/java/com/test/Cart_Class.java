package com.test;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class Cart_Class {
	WebDriver driver;
	
	String ExpectedResult;
	@BeforeClass
	public void Initialize()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://automationpractice.com/index.php");
		driver.manage().window().maximize();	
	}
	
	@Test(priority=0)
	public void AddCart() throws InterruptedException
	{
	

	driver.findElement(By.xpath("//a[@title='Women']")).click();
	
	 WebElement womenTab=driver.findElement(By.linkText("WOMEN"));
	 
	WebElement casulDressTab=driver.findElement(By.xpath("//a[@href='http://automationpractice.com/index.php?id_category=9&controller=category'  and text()='Casual Dresses']"));
	
	Actions act=new Actions(driver);
	act.moveToElement(womenTab).click(casulDressTab).build().perform();
	
	
	
	
	//driver.findElement(By.xpath("//span[text()='Add to cart']")).click();
	
//Add to Chart
	
	driver.findElement(By.linkText("Printed Dress")).click();						
	
	
	driver.findElement(By.xpath("//span[text()='Add to cart']")).click();
	
	//Proceed To Checkout
	
	WebElement proceedToCheckoutTab=driver.findElement(By.xpath("//div[@class='clearfix']"));
	
	WebElement btnToCheckoutTab=driver.findElement(By.xpath("//span[@class='continue btn btn-default button exclusive-medium']"));
	
	
	
	

	driver.findElement(By.xpath("//b[text()='Cart']")).click();
	driver.findElement(By.xpath("//a[@title='View my shopping cart']")).click();
	
	Thread.sleep(2000);
	
	
	
	

	}
	
	
	@Test(priority=1)
	public void CountElements() throws InterruptedException
	{
		
		 // List<WebElement> elements=driver.findElements(By.xpath("//*[contains(@class,'sf-with-ul')]"));
		  
		 // List<WebElement> elements=driver.findElements(By.xpath("//li[./a[contains(@class,'sf-with-ul')]]"));
		 // List<WebElement> elements=driver.findElements(By.xpath("//div[@id='block_top_menu']//a[@title]"));
		  
		  //List<WebElement> elements=driver.findElements(By.xpath("//div[@id='block_top_menu' and @class='sf-contener clearfix col-lg-12']//a[@class='sf-with-ul']"));
		  
		List<WebElement> elements=driver.findElements(By.xpath("//div[@id='block_top_menu']//a[@title='T-shirts']"));
		    int sizeofMainMenu=elements.size();
		  System.out.println("Size of MenuItem" + sizeofMainMenu);
		 	

			for (int i = 0; i <sizeofMainMenu; i++) 
			{

				String linktext = elements.get(i).getText();
				System.out.println("Menu items are :" + linktext);
				 Thread.sleep(5000);
			}
			
		 	
		 	List<WebElement> casulDressTab=driver.findElements(By.xpath("//ul[@class='submenu-container clearfix first-in-line-xs']"));
			
		 	  int sizeofcasulDressTab=casulDressTab.size();
			  System.out.println(sizeofcasulDressTab);			  
			
			  Thread.sleep(4000);
			  
			  for (int i = 0; i <sizeofcasulDressTab; i++) 
				{

					String linktext1 = casulDressTab.get(i).getText();
					System.out.println("Menu items are :" + linktext1);
					 Thread.sleep(5000);
				}
	      
	      
	}
	@Test(priority=2)
	public void PriceofElements() throws InterruptedException
	{
		
		WebElement PriceofElement=driver.findElement(By.xpath("//span[@id='total_price']"));
		  ExpectedResult = PriceofElement.getText();
		 Thread.sleep(2000);
		
	}
	
	@Test(priority=3)
	public void TotalPriceofElements() throws InterruptedException
	{
		Actions actTotalPrice=new Actions(driver);
		WebElement TotalCart=driver.findElement(By.xpath("//b[text()='Cart']"));
		 Thread.sleep(2000);
	
		 Actions act=new Actions(driver);
		act.moveToElement(TotalCart).build().perform(); 
		
		 Thread.sleep(2000);
		String ActualPrice= driver.findElement(By.xpath("//span[@class='price cart_block_total ajax_block_cart_total']")).getText();
		
		System.out.println("Total price of Items: " +ActualPrice);
		 Thread.sleep(2000);
		 
		 //Total number of Items in Cart
		 String Actual_NoofItems=driver.findElement(By.xpath("//span[@class='ajax_cart_quantity']")).getText();
		 System.out.println("Number of Items in Cart: " + Actual_NoofItems);
		 Assert.assertNotNull(Actual_NoofItems);
		 
		 
		 String Expected_NoofItems=driver.findElement(By.xpath("//span[@id='summary_products_quantity']")).getText();
		 
		 Assert.assertNotEquals(Actual_NoofItems,Expected_NoofItems);
	
		Assert.assertEquals(ActualPrice, ExpectedResult);
		 //Assert.assertNotNull(object);
		 
	}
	
	@AfterClass
	public void cleanUp()
	{

		driver.close();
	}
	

}

