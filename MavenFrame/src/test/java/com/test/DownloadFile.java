package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DownloadFile {
WebDriver driver;
	
	String ExpectedResult;
	@BeforeClass
	public void Initialize()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/");
		driver.manage().window().maximize();
		
		//driver.findElement(By.xpath(".//*[@id='program-header']/div[4]/a[1]")).click();
	}
	@Test
	public void FileDownload()
	{
		driver.findElement(By.xpath("//a[text()='File Download']")).click();
		
		driver.findElement(By.xpath("//a[@href='download/test1.jpg']")).click();
	}
	
}
