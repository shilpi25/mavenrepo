package com.test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
@Listeners(com.test.ListnerTest.class)
public class ListnerClass {
    WebDriver driver= new FirefoxDriver();

    // Test to pass as to verify listeners .
    @Test
    public void Login()
    {
        driver.get("https://www.facebook.com/");
        driver.findElement(By.name("email")).sendKeys("abcd");
        driver.findElement(By.name("pass")).sendKeys("efg123");
        driver.findElement(By.name("login")).click();
    }

    // Forcefully failed this test as to verify listener.
    @Test
    public void TestToFail()
    {
        System.out.println("This method to test fail");
        Assert.assertTrue(false);
    }
}
