package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;

public class ContactUs {
	
	@BeforeClass
	public void Initialize()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://automationpractice.com/index.php");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//a[text()='Contact us']")).click();
		
		
	}
	
	WebDriver driver;

	@DataProvider(name="Credential")
	public static Object[][] credentials()
	{
	return new Object[][] {{"abc@mailinator.com" , "Pass123"} ,{"def@gmail.com","123pwd"},{"efg@yahoo.com","welcome"}};
	}

	@Test(dataProvider = "Credential" )
	
	public void LoginFacebook(String Username , String OrderRef) throws InterruptedException
	{
		
		WebElement 	Heading=driver.findElement(By.xpath("//select[@id='id_contact']"));
		Select subHeading=new Select(Heading);
		subHeading.selectByIndex(1);
	
		driver.findElement(By.id("email")).clear();

	driver.findElement(By.id("email")).sendKeys(Username);
	driver.findElement(By.name("id_order")).clear();

	driver.findElement(By.name("id_order")).sendKeys(OrderRef);
	
	Thread.sleep(3000);
	
	WebElement uploadElement = driver.findElement(By.id("fileUpload"));

    // enter the file path onto the file-selection input field
    uploadElement.sendKeys("C:\\Users\\91971\\Documents\\folder\\new.html");

	
	driver.findElement(By.xpath("//textarea[@name='message']")).sendKeys("test");
	
	
	
	driver.findElement(By.xpath("//span[text()='Send']")).click();
	

	Thread.sleep(8000);

	}
	
	@AfterClass
	public void cleanUp()
	{

		driver.close();
	}


}
